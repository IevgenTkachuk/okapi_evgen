package net.sf.okapi.filters.openxml;

import org.assertj.core.api.iterable.Extractor;

import net.sf.okapi.common.resource.ITextUnit;

public class OpenXMLTestHelpers {
    public static Extractor<ITextUnit, Object> textUnitSourceExtractor() {
        return new Extractor<ITextUnit, Object>() {
            @Override
            public Object extract(ITextUnit input) {
                return input.getSource().toString();
            }
        };
    }
}
